# Backend Scala test task 
Name: Hrybakin Oleksandr

 to run with sbt:

```
   sbt "run original.html changed.html [original-element-id]"
```

 Or use binaries:

```
   ./pack/bin/crawler original.html changed.html [original-element-id]
   
   # OR
   
   java -cp "./pack/lib/*" task.crawler.CrawlerRunner original.html changed.html [original-element-id]
```




 
