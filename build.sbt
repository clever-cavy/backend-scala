import sbt.Keys.mainClass

enablePlugins(PackPlugin)
lazy val testtask = (project in file(".")).
  settings(

    organization := "task",
    name := "crawler",
    version := "0.0.1",
    scalaVersion := "2.11.8",

    resolvers ++= Seq(
      Resolver.sonatypeRepo("releases"),
      Resolver.jcenterRepo
    ),

    addCompilerPlugin(
      "org.scalamacros" % "paradise" % "2.1.0" cross CrossVersion.full
    ),

    //util dependencies
    libraryDependencies ++= Seq(
      "org.jsoup" % "jsoup" % "1.11.2",
      "com.typesafe.scala-logging" %% "scala-logging" % "3.8.0",
      "org.slf4j" % "slf4j-simple" % "1.7.26",
      "org.scalatest" %% "scalatest" % "3.0.8" % "test"
    ),


    fork in Test := true,
    parallelExecution in Test := false,
    javaOptions ++= Seq("-Xms512M", "-Xmx2048M", "-XX:MaxPermSize=2048M", "-XX:+CMSClassUnloadingEnabled"),

    mainClass in(Compile, run) := Some("task.crawler.CrawlerRunner"),

    packMain := Map("crawler" -> "task.crawler.CrawlerRunner")
  )