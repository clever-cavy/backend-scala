package task.crawler

import java.nio.file.Paths

import org.scalatest.{FlatSpec, Matchers}

class CrawlerTest extends FlatSpec with Matchers {
  def toFile(resource: String) = {
    val res = getClass.getClassLoader.getResource(resource)
    println(res)
    Paths.get(res.toURI).toFile
  }

  val originalFile = toFile("origin.html")
  val changedFile = toFile("sample-1.html")

  "everything" should "work in simple case" in {
    val crawler = new Crawler(originalFile, changedFile)
    val result = crawler.findElement("make-everything-ok-button")
    result shouldBe Some("html[] > body[] > div[wrapper] > div[page-wrapper] > div[] > div[] > div[] > div[]  attributes: Map(rel -> done, onclick -> javascript:window.okDone(); return false;, class -> btn btn-success, title -> Make-Button, href -> #check-and-ok)")
  }
}
