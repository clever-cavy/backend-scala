package task.crawler

import java.io.File

object CrawlerRunner extends App {
  val DefaultElementId = "make-everything-ok-button"

  val (originalFile, changedFile, elementId) = args match {
    case Array(path1, path2) =>
      (path1, path2, DefaultElementId)
    case Array(path1, path2, elementId) =>
      (path1, path2, elementId)
    case _ =>
      throw new RuntimeException(s"Wrong args: `${args.toList}`. Please, provide path to origin file and changed file separated by space")
  }

  val crawler = new Crawler(new File(originalFile), new File(changedFile))
  val result = crawler.findElement(elementId) match {
    case Some(result) => s"Best candidate: \n$result"
    case None => s"Found nothing similar to element with id $elementId"
  }
  println(result)
}
