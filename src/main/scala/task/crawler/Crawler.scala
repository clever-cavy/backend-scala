package task.crawler

import java.io.File

import com.typesafe.scalalogging.LazyLogging
import org.jsoup.Jsoup
import org.jsoup.nodes.{Attribute, Element}
import org.jsoup.select.Elements

import scala.collection.JavaConversions._
import scala.util.{Failure, Success, Try}

class Crawler(originalFile: File, changedFile: File) extends LazyLogging {

  import Crawler._

  def findElement(elementId: String): Option[String] = {
    findElementById(originalFile, elementId).map(ElementFeatures(_)).map { features =>
      val cssQueries = features.attributes
        .map { case (k, v) => s"""[$k="$v"]""" }
        .toList

      val candidates = for {
        cssQuery <- cssQueries
        elements <- findElementsByQuery(changedFile, cssQuery).toList
        features <- elements.iterator().toList.map(ElementFeatures(_))
      } yield features

      val topCandidates = candidates.distinct.sortBy(other => -features.matchedAttributesCount(other)).take(5)

      logger.info(s"Candidates: \n${topCandidates.mkString("\n")}")

      topCandidates
        .headOption.map(_.toString)
        .getOrElse("")
    }
  }

  private def findElementById(htmlFile: File, targetElementId: String): Option[Element] = Try {
    Jsoup.parse(htmlFile, CharsetName)
  }.map(_.getElementById(targetElementId)) match {
    case Success(element) => Option(element)
    case Failure(exception) =>
      logger.error(s"error occured while trying to find element $targetElementId in file $htmlFile:", exception)
      None
  }

  private def findElementsByQuery(htmlFile: File, cssQuery: String): Option[Elements] = Try {
    Jsoup.parse(htmlFile, CharsetName)
  }.map(_.select(cssQuery)) match {
    case Success(elements) => Option(elements)
    case Failure(exception) =>
      logger.error(s"error occured while trying to find element $cssQuery in file $htmlFile:", exception)
      None
  }
}

object Crawler {
  private val CharsetName = "UTF-8"

  case class ElementFeatures(id: String, xPath: String, name: String, parentId: String, text: String, attributes: Map[String, String]) {

    def matchedAttributesCount(other: ElementFeatures): Int = {
      var matched = 0
      for {
        (key, value) <- attributes
      } if (other.attributes.get(key).contains(value)) matched += 1

      if (parentId == other.parentId) matched += 1
      if (name == other.name) matched += 1
      if (text == other.text) matched += 1
      matched
    }

    override def toString: String = {
      s"$xPath $id attributes: $attributes"
    }
  }

  object ElementFeatures {
    def toMapEntry(attr: Attribute): (String, String) = attr.getKey -> attr.getValue

    def xPath(element: Element): String = {
      element.parents().map(node => s"${node.tagName}[${node.id}]").reverse.mkString(" > ")
    }

    def apply(element: Element): ElementFeatures = ElementFeatures(
      element.id,
      xPath(element),
      element.tagName(),
      element.parent().id(),
      element.text(),
      element.attributes().toList.map(toMapEntry).toMap
    )
  }

}
